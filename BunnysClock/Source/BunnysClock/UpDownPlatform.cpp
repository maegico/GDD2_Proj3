// Fill out your copyright notice in the Description page of Project Settings.

#include "BunnysClock.h"
#include "UpDownPlatform.h"


// Sets default values
AUpDownPlatform::AUpDownPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	root = CreateDefaultSubobject<UBoxComponent>(TEXT("root"));
	RootComponent = root;

	//sprite = CreateDefaultSubobject<>(TEXT("mesh"));
	//sprite->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void AUpDownPlatform::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUpDownPlatform::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

