// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "BunnysClock.h"
#include "BunnysClock.generated.dep.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBunnysClock() {}
	void UUpDown::StaticRegisterNativesUUpDown()
	{
	}
	IMPLEMENT_CLASS(UUpDown, 960928891);
	void AUpDownPlatform::StaticRegisterNativesAUpDownPlatform()
	{
	}
	IMPLEMENT_CLASS(AUpDownPlatform, 2296348470);
	void UUpDownPlats::StaticRegisterNativesUUpDownPlats()
	{
	}
	IMPLEMENT_CLASS(UUpDownPlats, 1021936309);
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	ENGINE_API class UClass* Z_Construct_UClass_UShapeComponent_NoRegister();

	BUNNYSCLOCK_API class UClass* Z_Construct_UClass_UUpDown_NoRegister();
	BUNNYSCLOCK_API class UClass* Z_Construct_UClass_UUpDown();
	BUNNYSCLOCK_API class UClass* Z_Construct_UClass_AUpDownPlatform_NoRegister();
	BUNNYSCLOCK_API class UClass* Z_Construct_UClass_AUpDownPlatform();
	BUNNYSCLOCK_API class UClass* Z_Construct_UClass_UUpDownPlats_NoRegister();
	BUNNYSCLOCK_API class UClass* Z_Construct_UClass_UUpDownPlats();
	BUNNYSCLOCK_API class UPackage* Z_Construct_UPackage_BunnysClock();
	UClass* Z_Construct_UClass_UUpDown_NoRegister()
	{
		return UUpDown::StaticClass();
	}
	UClass* Z_Construct_UClass_UUpDown()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UActorComponent();
			Z_Construct_UPackage_BunnysClock();
			OuterClass = UUpDown::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20B00080;


				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("Custom"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("ComponentReplication"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("UpDown.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("UpDown.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUpDown(Z_Construct_UClass_UUpDown, TEXT("UUpDown"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUpDown);
	UClass* Z_Construct_UClass_AUpDownPlatform_NoRegister()
	{
		return AUpDownPlatform::StaticClass();
	}
	UClass* Z_Construct_UClass_AUpDownPlatform()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage_BunnysClock();
			OuterClass = AUpDownPlatform::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_root = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("root"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(root, AUpDownPlatform), 0x0000000000080009, Z_Construct_UClass_UShapeComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("UpDownPlatform.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("UpDownPlatform.h"));
				MetaData->SetValue(NewProp_root, TEXT("Category"), TEXT("UpDownPlatform"));
				MetaData->SetValue(NewProp_root, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_root, TEXT("ModuleRelativePath"), TEXT("UpDownPlatform.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUpDownPlatform(Z_Construct_UClass_AUpDownPlatform, TEXT("AUpDownPlatform"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUpDownPlatform);
	UClass* Z_Construct_UClass_UUpDownPlats_NoRegister()
	{
		return UUpDownPlats::StaticClass();
	}
	UClass* Z_Construct_UClass_UUpDownPlats()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UActorComponent();
			Z_Construct_UPackage_BunnysClock();
			OuterClass = UUpDownPlats::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20B00080;


				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("Custom"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("ComponentReplication"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("UpDownPlats.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/UpDownPlats.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUpDownPlats(Z_Construct_UClass_UUpDownPlats, TEXT("UUpDownPlats"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUpDownPlats);
	UPackage* Z_Construct_UPackage_BunnysClock()
	{
		static UPackage* ReturnPackage = NULL;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), NULL, FName(TEXT("/Script/BunnysClock")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0xDF95DF6F;
			Guid.B = 0xB7585ABB;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif

PRAGMA_ENABLE_DEPRECATION_WARNINGS
