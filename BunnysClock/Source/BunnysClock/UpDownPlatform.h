// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
//#include "PaperSpriteComponent.h"
#include "UpDownPlatform.generated.h"

UCLASS()
class BUNNYSCLOCK_API AUpDownPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUpDownPlatform();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
		UShapeComponent* root;

	/*UPROPERTY(EditAnywhere)
		class UPaperSpriteComponent* sprite;*/
		//UStaticMeshComponent* mesh;
	
};
